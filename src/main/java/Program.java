package main.java;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.*;

import static java.time.temporal.ChronoUnit.MILLIS;


class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  //creating a Scanner object
        boolean finished = false;

        while (!finished) {  //checking if the user wants to quit or continuing
            int option = showMainMenu(scanner);
            if (option == 4) {
                finished = true;
            } else {
                executeOption(scanner, option);
            }
        }
        scanner.close();
    }

    private static int showMainMenu(Scanner scanner) {  //shows the main menu and returns the option chosen by the user
        boolean approved = false;
        int option = 0;

        while(!approved) {
            //the main menu
            System.out.println("Choose one of the following options:");
            System.out.println("1 - List all file names");
            System.out.println("2 - List all files based on their extension");
            System.out.println("3 - Get info about Daracula.txt");
            System.out.println("4 - Quit");

            try {
                option = scanner.nextInt();  //gets user input
            } catch (InputMismatchException ex) {  //if the user puts anything else but an integer the menu will show again
                System.out.println("You typed something that was not allowed");
                scanner.nextLine();
                option = showMainMenu(scanner);
            }
            if (option >= 1 && option <= 4) {  //checking if the option is valid
                approved = true;
            } else {
                System.out.println("Please choose a valid option");
                scanner.nextLine();
                option = showMainMenu(scanner);
                if (option >= 1 && option <= 4) {
                    approved = true;
                }
            }
        }
        return option;
    }

    private static void executeOption(Scanner scanner, int option) {  //executes the option chosen by the user
        LocalTime before;  //for the logging
        LocalTime after;  //for the logging
        String log;  //for the logging

        switch (option) {
            case 1:
                before = java.time.LocalTime.now();
                showFiles();
                after = java.time.LocalTime.now();
                log = "\n" + java.time.LocalTime.now() + ": Option " + option + " was successful. The function took " + MILLIS.between(before, after) + "ms to execute";
                writeToLog(log);
                goBackMenu(scanner);
                break;
            case 2:
                ArrayList<String> extensions = getAllExtension();
                int exOption = showExtensionMenu(scanner, extensions);
                if (exOption <= extensions.size()) {
                    before = java.time.LocalTime.now();
                    String extension = extensions.get(exOption - 1);
                    showFiles(extension);
                    after = java.time.LocalTime.now();
                    log = "\n" + java.time.LocalTime.now() + ": Option " + option + " was successful. The function took " + MILLIS.between(before, after) + "ms to execute";
                    writeToLog(log);
                    goBackMenu(scanner);
                }
                break;
            case 3:
                System.out.println("Please type the word you want to search through the file");
                String word = "";
                try {
                    word = scanner.next();  //dets the user input for word to search with
                } catch (InputMismatchException e) {  //if the user puts anything tha will make the program stop
                    System.out.println("You typed something that was not allowed");
                }
                before = java.time.LocalTime.now();
                getDraculaInfo(word);
                after = java.time.LocalTime.now();
                log = "\n" + java.time.LocalTime.now() + ": Option " + option + " was successful. The function took " + MILLIS.between(before, after) + "ms to execute";
                writeToLog(log);
                goBackMenu(scanner);
                break;
        }
    }

    private static int showExtensionMenu(Scanner scanner, ArrayList<String> extensions) {  //menu to choose extension
        System.out.println("Please choose one of the following extensions");
        for (int i = 0; i < extensions.size(); i++) {  //printing all the options
            System.out.println((i + 1) + " - " + extensions.get(i));
        }
        System.out.println((extensions.size() + 1) + " - Go back");
        boolean approved = false;
        int exOption = 0;
        while(!approved) {
            try {
                exOption = scanner.nextInt();  //gets user input
            } catch (InputMismatchException ex) {  //if the user puts anything else but an integer the program will stop
                System.out.println("You typed something that was not allowed");
                scanner.nextLine();
                exOption = showExtensionMenu(scanner, extensions);
            }
            if (exOption >= 1 && exOption <= extensions.size() + 1) {
                approved = true;
            } else {
                System.out.println("Please choose a valid option");
                scanner.nextLine();
                exOption = showExtensionMenu(scanner, extensions);
                if (exOption >= 1 && exOption <= extensions.size() + 1) {
                    approved = true;
                }
            }
        }
        return exOption;
    }

    private static void goBackMenu(Scanner scanner) {  //menu to get back to main menu
        boolean back = false;
        System.out.println("To go back type b");
        while(!back) {
            try {
                String b = scanner.next();  //gets user input
                if (b.equalsIgnoreCase("b")) {  //checking if the user input is valid
                    back = true;
                } else {
                    System.out.println("Please type b to go back");
                }
            } catch (InputMismatchException ex) {  //if the user puts anything that isn't allowed
                System.out.println("Please type b to go back");
            }
        }
    }

    private static void showFiles() {  //printing all files in resources
        File dir = new File("resources");  //making the directory a File object
        String[] filenames = dir.list();  //listing all files in resources

        if (filenames != null) {
            for (String filename : filenames) {
                System.out.println(filename);
            }
        } else {
            System.out.println("There was no files in the resource directory");
        }
    }
    
    private static ArrayList<String> getAllExtension() {  //returns all the extensions from the files in resources
        File dir = new File("resources");  //making the directory a File object
        String[] filenames = dir.list(); //listing all the files from resources
        ArrayList<String> extensions = new ArrayList<>();
        boolean duplicate = false;

        if (filenames != null) {
            extensions.add(filenames[0].substring(filenames[0].lastIndexOf('.') + 1));  //adds the first extension
            for (int i = 1; i < filenames.length; i++) {
                for (int j = 0; j < extensions.size(); j++) {
                    if (filenames[i].substring(filenames[i].lastIndexOf('.') + 1).equals(extensions.get(j).substring(extensions.get(j).lastIndexOf('.') + 1))) {  //checking for duplicates
                        duplicate = true;
                    }
                }
                if (!duplicate) {  //adds extension to array if not duplicate
                    extensions.add(filenames[i].substring(filenames[i].lastIndexOf('.') + 1));
                }
                duplicate = false;
            }
        } else {
            System.out.println("There was no files in the resource directory");
        }
        return extensions;
    }

    private static void showFiles(String extension) {  //printing all files in resources based on a extension given by the user
        File dir = new File("resources");  //making the directory a File object
        String[] filenames = dir.list(); //listing all the files from resources

        boolean noFiles = true;
        if (filenames != null) {
            for (String filename : filenames) {
                if (filename.endsWith(extension)) {  //checking for match between extensions
                    noFiles = false;
                    System.out.println(filename);
                }
            }
            if (noFiles) {
                System.out.println("There was no files with that extension in the resource directory");
            }
        } else {
            System.out.println("There was no files in the resource directory");
        }
    }

    private static void getDraculaInfo(String word) {  //printing the name, size, amount of lines and amount of appearance from a word given by the user in Dracula.txt
        File dir = new File("resources");  //making the directory a File object
        String[] filenames = dir.list(); //listing all the files from resources

        boolean noFiles = true;
        if (filenames != null) {
            for (String filename : filenames) {
                if (filename.equals("Dracula.txt")) {  //finding the right file
                    noFiles = false;
                    System.out.println(filename);
                }
            }
            if (noFiles) {
                System.out.println("There was no files with that extension in the resource directory");
            }
        } else {
            System.out.println("There was no files in the resource directory");
        }

        //printing the size of the file
        try (FileInputStream fileInputStream = new FileInputStream("resources/Dracula.txt")) {
            int byteCount = 0;
            while(fileInputStream.read() != -1) {  //counting bytes
                byteCount++;
            }
            System.out.println("The file is " + byteCount + " Bytes");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //printing the number of lines from the file
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("resources/Dracula.txt"))) {
            int lines = 0;

            while (bufferedReader.readLine() != null) {  //counting lines
                lines++;
            }
            System.out.println("It contains " + lines + " lines");
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //printing the number of appearance of a word from the file
        try(Scanner scanner = new Scanner(new File("resources/Dracula.txt"))){
            int wordCount = 0;
            scanner.useDelimiter(" |\\.|\\,|\\:|\\-|\\_|\\[|\\]|\\(|\\)|\\!|\\?");  //splitting the text by these signs

            while(scanner.hasNext()){
                if(scanner.next().equalsIgnoreCase(word)){  //comparing words
                    wordCount++;
                }
            }
            if (wordCount == 0) {
                System.out.println(word + " does not exists in the file");
            } else {
                System.out.println(word + " appears " + wordCount + " times in the file");
            }
        }
        catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private static void writeToLog(String log) { //writes the log to Log.txt
        try {
            Files.write(Paths.get("Log.txt"), log.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}