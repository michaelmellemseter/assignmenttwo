# AssignmentTwo

A JAVA console application that can be used to manage and manipulate files

## Description

The user can choose between three options:

* Show all files from the resource directory
* Show all files with a specific extension given by the user
* Get information about Dracula.tx
    * Name of the file
    * How big the file is (in bytes)
    * How many lines the file contains
    * How many appearances of a word given by the user there is in the file

The resource directory which contains Dracula.txt is given as part of this assignment

